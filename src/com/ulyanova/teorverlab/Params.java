package com.ulyanova.teorverlab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Params {
	private static String PARAM_PREFIX = "-";
	private static String U_PREFIX = "u";
	private static String G_PREFIX = "g";
	private static String XA_PREFIX = "xA";
	private static String XB_PREFIX = "xB";
	private static String YA_PREFIX = "yA";
	private static String YB_PREFIX = "yB";
	private static String N_PREFIX = "N";

	private double u;
	private double g;
	private double xA;
	private double xB;
	private double yA;
	private double yB;
	private int N;


	public static Params parse(String[] args) {
		List<String> paramsList = new ArrayList<>(Arrays.asList(args));
		Params params = new Params();
		return params.parse(paramsList);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Params{");
		sb.append("u=").append(u);
		sb.append(", g=").append(g);
		sb.append(", x=").append("[").append(xA).append(";").append(xB).append("]");
		sb.append(", y=").append("[").append(yA).append(";").append(yB).append("]");
		sb.append(", N=").append(N);
		sb.append('}');
		return sb.toString();
	}

	private Params parse(List<String> paramsList) {

		for (String param : paramsList) {
			parseValue(param);
		}

		return this;
	}

	private void parseValue(String param) {
		if (!param.startsWith(PARAM_PREFIX)) {
			return;
		}
		param = param.substring(PARAM_PREFIX.length());

		if (param.startsWith(U_PREFIX)) {
			String paramValue = param.substring(U_PREFIX.length());
			u = parseToDouble(paramValue);
		} else if (param.startsWith(G_PREFIX)) {
			String paramValue = param.substring(G_PREFIX.length());
			g = parseToDouble(paramValue);
		} else if (param.startsWith(XA_PREFIX)) {
			String paramValue = param.substring(XA_PREFIX.length());
			xA = parseToDouble(paramValue);
		} else if (param.startsWith(XB_PREFIX)) {
			String paramValue = param.substring(XB_PREFIX.length());
			xB = parseToDouble(paramValue);
		} else if (param.startsWith(YA_PREFIX)) {
			String paramValue = param.substring(YA_PREFIX.length());
			yA = parseToDouble(paramValue);
		} else if (param.startsWith(YB_PREFIX)) {
			String paramValue = param.substring(YB_PREFIX.length());
			yB = parseToDouble(paramValue);
		} else if (param.startsWith(N_PREFIX)) {
			String paramValue = param.substring(N_PREFIX.length());
			N = parseToInt(paramValue);
		}
	}

	private double parseToDouble(String value) {
		return Double.valueOf(value);
	}

	private int parseToInt(String value) {
		return Integer.valueOf(value);
	}

	public double getU() {
		return u;
	}

	public double getG() {
		return g;
	}

	public double getxA() {
		return xA;
	}

	public double getxB() {
		return xB;
	}

	public double getyA() {
		return yA;
	}

	public double getyB() {
		return yB;
	}

	public int getN() {
		return N;
	}
}
