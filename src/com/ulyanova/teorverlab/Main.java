package com.ulyanova.teorverlab;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

	public static void main(String[] args) {

		System.out.println("Generation values of the Gaussian Function 1/(g*sqrt(2*pi)) * e^-((x-u)^2/2g^2)");
		Params params = Params.parse(args);
		System.out.println("with " + params);

		Range rangeX = new Range(params.getxA(), params.getxB());
		Range rangeY = new Range(params.getyA(), params.getyB());

		GaussianFunction function = new GaussianFunction(params.getU(), params.getG());
		GaussianFunctionGenerator generator = new GaussianFunctionGenerator(rangeX, rangeY, params.getN(), function);
		generator.generate();

		double[] values = generator.getResult();

		print(values);

		exportToCSV(values);

	}

	private static void print(double[] values) {
		for (double value : values) {
			DecimalFormat df = new DecimalFormat("#.##");
			System.out.println(df.format(value));
		}
	}

	private static void exportToCSV(double[] results) {
		if (null == results) return;
		File file = getFile();

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

			System.out.println("Exporting results to file " + file.getName());
			DecimalFormat df = new DecimalFormat("#.##");
			for (double result : results) {
				writer.write(df.format(result));
				writer.write(";\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static File getFile() {
		String csvExtension = ".csv";
		String resultPrefix = "result_";
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
		String fileName = resultPrefix + now.format(formatter) + csvExtension;
		return new File(fileName);
	}
}
