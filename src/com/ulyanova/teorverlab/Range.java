package com.ulyanova.teorverlab;

/**
 *
 */
public class Range {
	private double begin;
	private double end;
	private double length;

	public Range(double a, double b) {
		this.begin = a;
		this.end = b;
		this.length = end - begin;
	}

	public double scale(double value) {
		if (value < 0) {
			return begin;
		} else if (value > 1.0) {
			return end;
		} else {
			return begin + length * value;
		}
	}

	public double getBegin() {
		return begin;
	}

	public double getEnd() {
		return end;
	}

	public double getLength() {
		return length;
	}
}
