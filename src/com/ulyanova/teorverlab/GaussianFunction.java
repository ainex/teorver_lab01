package com.ulyanova.teorverlab;

/**
 * f(x)=1/(g√2π) * e^(-[(x-u)]^2/(2*g^2))
 * a = 1/(g√2π), b = (2*g^2);
 */
public class GaussianFunction {
	private double u;
	private double g;
	private double a;
	private double b;


	public GaussianFunction() {
		this(0, 1.0);
	}

	public GaussianFunction(double u, double g) {
		this.u = u;
		this.g = g;
		this.a = calculateParamA();
		this.b = calculateParamB();
	}

	public double getValue(double x) {
		return a * Math.pow(Math.E, -1 * (Math.pow((x - u), 2) / b));
	}

	private double calculateParamA() {
		return 1 / (g * Math.sqrt(2 * Math.PI));
	}

	private double calculateParamB() {
		return 2 * g * g;
	}
}
