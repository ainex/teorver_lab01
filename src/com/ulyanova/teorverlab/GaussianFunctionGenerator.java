package com.ulyanova.teorverlab;

import java.util.Random;

/**
 *
 */
public class GaussianFunctionGenerator {
	private Range rangeX;
	private Range rangeY;
	private int amount;
	private GaussianFunction function;
	private double[] result;

	public GaussianFunctionGenerator(Range rangeX, Range rangeY, int amount, GaussianFunction function) {
		this.rangeX = rangeX;
		this.rangeY = rangeY;
		this.amount = amount;
		this.result = new double[amount];
		this.function = function;
	}

	public void generate() {
		int i = 0;
		Random random = new Random();
		double x, y;

		while (i < amount) {
			x = rangeX.scale(random.nextDouble());
			y = rangeY.scale(random.nextDouble());
			double maxY = function.getValue(x);
			if (Double.compare(maxY, y) > 0) {
				x = Math.floor(x * 100) / 100;
				result[i] = x;
				i++;
			}
		}
	}

	public double[] getResult() {
		return result;
	}
}
